Name:                 zsh
Version:              5.9
Release:              4
Summary:              A shell designed for interactive use
License:              MIT
URL:                  https://zsh.sourceforge.net
Source0:              https://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.xz

# There are five startup files that zsh will read commands from
# http://zsh.sourceforge.net/Intro/intro_3.html
Source1:              zlogin.rhs
Source2:              zlogout.rhs
Source3:              zprofile.rhs
Source4:              zshrc.rhs
Source5:              zshenv.rhs
Source6:              dotzshrc
Source7:              dotzprofile
# do not use egrep in tests to make them pass again
Patch1: 0001-zsh-5.9-do-not-use-egrep-in-tests.patch
# Upstream commit ab4d62eb975a4c4c51dd35822665050e2ddc6918
Patch2: 0002-zsh-Use-int-main-in-test-c-codes.patch
# upstream commit a84fdd7c8f77935ecce99ff2b0bdba738821ed79
Patch3: 0003-zsh-fix-module-loading-problem-with-full-RELRO.patch
# upstream commit 1b421e4978440234fb73117c8505dad1ccc68d46
Patch4: 0004-zsh-enable-PCRE-locale-switching.patch
# upstream commit b62e911341c8ec7446378b477c47da4256053dc0 and 10bdbd8b5b0b43445aff23dcd412f25cf6aa328a
Patch5: 0005-zsh-port-to-pcre2.patch
# upstream commit ecd3f9c9506c7720dc6c0833dc5d5eb00e4459c4
Patch6: 0006-zsh-support-texinfo-7.0.patch
# upstream commit 4c89849c98172c951a9def3690e8647dae76308f
Patch7: 0007-zsh-configure-c99.patch
# upstream commit d3edf318306e37d2d96c4e4ea442d10207722e94
Patch8: 0008-zsh-deletefilelist-segfault.patch

BuildRequires:        autoconf coreutils gawk gdbm-devel libcap-devel make
BuildRequires:        ncurses-devel pcre2-devel sed texinfo hostname gcc

Requires(post):       grep
Requires(postun):     coreutils grep

Provides:             /bin/zsh

%description
The zsh is a shell designed for interactive use, and it is also a powerful scripting language. Many of
the useful features of bash, ksh, and tcsh were incorporated into zsh. It can match files by file extension
without running an external program, share command history with any shell, and more.

%package              help
Summary:              zsh shell manual in html format
BuildArch:            noarch

Provides:             zsh-html = %{version}-%{release}
Obsoletes:            zsh-html < %{version}-%{release}

%description          help
This package contains the zsh manual in html format.

%prep
%autosetup -p1
autoreconf -fiv

sed -e 's|^\.NOTPARALLEL|#.NOTPARALLEL|' -i 'Config/defs.mk.in'

%build
%undefine _strict_symbol_defs_build

%configure \
    --enable-etcdir=%{_sysconfdir} \
    --with-tcsetpgrp \
    --enable-maildir-support \
    --enable-pcre

make -C Src headers
make -C Src -f Makemod zsh{path,xmod}s.h version.h
%make_build all html

%check
# avoid unnecessary failure of the test-suite in case ${RPS1} is set
unset RPS1

%make_build check

%install
%make_install install.info \
  fndir=%{_datadir}/%{name}/%{version}/functions \
  sitefndir=%{_datadir}/%{name}/site-functions \
  scriptdir=%{_datadir}/%{name}/%{version}/scripts \
  sitescriptdir=%{_datadir}/%{name}/scripts \
  runhelpdir=%{_datadir}/%{name}/%{version}/help

rm -f %{buildroot}%{_bindir}/zsh-%{version}
rm -f %{buildroot}%{_infodir}/dir

mkdir -p %{buildroot}%{_sysconfdir}
for i in %{S:1} %{S:2} %{S:3} %{S:4} %{S:5}; do
    install -m 644 $i %{buildroot}%{_sysconfdir}/"$(basename $i .rhs)"
done

mkdir -p %{buildroot}%{_sysconfdir}/skel
install -m 644 %{S:6} %{buildroot}%{_sysconfdir}/skel/.zshrc
install -m 644 %{S:7} %{buildroot}%{_sysconfdir}/skel/.zprofile

# This is just here to shut up rpmlint, and is very annoying.
# Note that we can't chmod everything as then rpmlint will complain about
# those without a she-bang line.
for i in checkmail harden run-help test-repo-git-rebase-{apply,merge} zcalc zkbd; do
    sed -i -e 's!/usr/local/bin/zsh!%{_bindir}/zsh!' \
    %{buildroot}%{_datadir}/zsh/%{version}/functions/$i
    chmod +x %{buildroot}%{_datadir}/zsh/%{version}/functions/$i
done

%post
if [ "$1" = 1 ]; then
  if [ ! -f %{_sysconfdir}/shells ] ; then
    echo "%{_bindir}/%{name}" > %{_sysconfdir}/shells
    echo "/bin/%{name}" >> %{_sysconfdir}/shells
  else
    grep -q "^%{_bindir}/%{name}$" %{_sysconfdir}/shells || echo "%{_bindir}/%{name}" >> %{_sysconfdir}/shells
    grep -q "^/bin/%{name}$" %{_sysconfdir}/shells || echo "/bin/%{name}" >> %{_sysconfdir}/shells
  fi
fi

%postun
if [ "$1" = 0 ] && [ -f %{_sysconfdir}/shells ] ; then
  sed -i '\!^%{_bindir}/%{name}$!d' %{_sysconfdir}/shells
  sed -i '\!^/bin/%{name}$!d' %{_sysconfdir}/shells
fi

%files
%license LICENCE
%doc README Etc/* FEATURES MACHINES NEWS
%attr(755,root,root) %{_bindir}/zsh
%{_datadir}/zsh
%{_libdir}/zsh
%config(noreplace) %{_sysconfdir}/skel/.z*
%config(noreplace) %{_sysconfdir}/z*

%files help
%doc Doc/*.html
%{_mandir}/*/*
%{_infodir}/*

%changelog
* Mon Oct 07 2024 Funda Wang <fundawang@yeah.net> - 5.9-4
- cleanup spec
- sync patches from fedora

* Mon Jan 30 2023 Cao Jingbo <caojb@chinatelecom.cn> - 5.9-3
- Fix when use grep 3.8 build error.

* Tue Jan 17 2023 dillon chen < dillon.chen@gmail.com> - 5.9-2
- /usr/local/bin/zsh => /bin/zsh

* Sun Oct  9 2022 dillon chen < dillon.chen@gmail.com> - 5.9-1
- update to 5.9

* Tue Mar 1 2022 wangjie <wangjie375@h-partners.com> - 5.8-3
- Type: CVE
- ID: CVE-2021-45444
- SUG: NA
- DESC: fix CVE-2021-45444

* Sat Mar 20 2021 shenyangyang <shenyangyang4@huawei.com> - 5.8-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add -fstack-protector-strong for so file

* Fri Jan 29 2021 zoulin <zoulin13@huawei.com> - 5.8-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 5.8

* Wed Jun 24 2020 xuping <xuping21@huawei.com> - 5.7.1-5
- Type:cves
- ID:CVE-2019-20044
- SUG:NA
- DESC:fix CVE-2019-20044

* Thu Feb 6 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.7.1-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add buildrequires of gcc and make

* Mon Feb 3 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.7.1-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:fix missing files 

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.7.1-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise buildrequires

* Wed Jan 8 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.7.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 5.7.1

* Wed Dec 18 2019 jiangchuangang <jiangchuangang@huawei.com> - 5.6.2-3
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:Synchronize a patch

* Wed Sep 18 2019 dongjian <dongjian13@huawei.com> - 5.6.2-2
- modify summary
